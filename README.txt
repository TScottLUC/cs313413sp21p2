TestList.java:

    Q: public void setUp() throws Exception {
        list = new ArrayList<Integer>(); TO DO also try with a LinkedList - does it make any difference behaviorally? (ignore performance)
        Answer: This does not make a difference behaviorally, all tests still pass.

    Q: list.remove(5); // TO DO answer: what does this method do?
        Answer: In testRemoveObject() of TestList.java, the above code removes the 5th index in the list, which would be the int 77.

    Q: list.remove(Integer.valueOf(5)); // TO DO answer: what does this one do?
        Answer: In testRemoveObject() of TestList.java, the above code removes the first occurrence of the int 5 in the list, which would be the 4th index

TestIterator.java:

    Q: public void setUp() throws Exception {
        list = new ArrayList<Integer>(); TO DO also try with a LinkedList - does it make any difference behaviorally? (ignore performance)
        Answer: This does not make a difference behaviorally, all tests still pass.

    Q: while (i.hasNext()) {
             if (i.next() == 77) {
               i.remove();}} // TO DO what happens if you use list.remove(Integer.valueOf(77))?
       Answer: If you use list.remove(Integer.valueOf(77)) instead, the test fails. It appears to exit the while loop without
                removing all values of 77, and only removing the first occurrence.

TestPerformance.java:

    Running times: (REPS=1_000_000 for all tests)
        SIZE = 10/100/1000/10000:
            testLinkedListAddRemove: 28 ms/30 ms/31 ms/33 ms
            testArrayListAddRemove: 39 ms/47 ms/133 ms/1s 144ms
            testLinkedListAccess: 12 ms/25 ms/276 ms/3s 560 ms
            testArrayListAccess: 10 ms/11 ms/12 ms/12 ms

    Q: which of the two lists performs better for each method as the size increases?
        Answer: The Linked List performs better for AddRemove, while the ArrayList
                performs better for Access.
